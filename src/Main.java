import controllers.FileController;

import java.util.Scanner;

public class Main {

    public static final int SIZE_ARRAY = 20;
    public static Scanner keyboard = new Scanner(System.in);

    public static int position = 0;
    public static double[] results = new double[SIZE_ARRAY];
    public static FileController fileController = new FileController();

    public static void main(String[] args) {
        menu();
        int option;
        do {
            System.out.print("Seleccione una opción en el menú principal: ");
            try {
                option = readInteger();
                switch (option) {
                    case 0 -> System.out.println("El programa ha finalizado");
                    case 1 -> sum();
                    case 2 -> subtract();
                    case 3 -> multiply();
                    case 4 -> divide();
                    case 5 -> printHistory();
                    default -> System.out.println("  ¡Opción no disponible en el menú principal!");
                }
            } catch (NumberFormatException e) {
                System.out.println("  ¡La opción ingresada debe ser un número entero!");
                option = -1;
            }
        } while (option != 0);
    }

    public static void menu() {
        System.out.println("╔═══════════════════════════════════════════╗");
        System.out.println("╠----------------Calculadora----------------╣");
        System.out.println("║═══════════════════════════════════════════║");
        System.out.println("║   1. Sumar                                ║");
        System.out.println("║   2. Restar                               ║");
        System.out.println("║   3. Multiplicar                          ║");
        System.out.println("║   4. Dividir                              ║");
        System.out.println("║   5. Imprimir historial                   ║");
        System.out.println("║   0. Salir                                ║");
        System.out.println("╚═══════════════════════════════════════════╝\n");
    }

    private static void sum() {
        double[] numbers = readNumbers("sumar");
        double sum = 0;
        for (double number : numbers) {
            sum += number;
        }
        System.out.println("\n  La suma de los números ingresados es: " + sum);
        addItem(sum);

    }

    private static void subtract() {
        double[] numbers = readNumbers("restar");
        double subtract = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            subtract -= numbers[i];
        }
        System.out.println("\n  La resta de los números ingresados es: " + subtract);
        addItem(subtract);
    }

    private static void multiply() {
        double[] numbers = readNumbers("multiplicar");
        double multiply = 1;
        for (double number : numbers) {
            multiply *= number;
        }
        System.out.println("\n  El producto de los números ingresados es: " + multiply);
        addItem(multiply);
    }

    private static void divide() {
            double dividend = readDoubleValid(0);
            double divider = readDoubleValid(1);
            if(divider == 0){
                System.out.println("  ¡El divisor no puede ser 0!");
            } else{
                System.out.println("\n  El cociente de los números ingresados es: " + (dividend / divider));
                addItem(dividend / divider);
            }
    }

    private static double[] readNumbers(String operation) {
        System.out.print("  Ingrese la cantidad de números que desea " + operation + ": ");
        double[] tempNumbers = null;
        boolean valid;
        do {
            try {
                int limit = readInteger();
                tempNumbers = new double[limit];
                for (int i = 0; i < limit; i++) {
                    tempNumbers[i] = readDoubleValid(i);
                }
                valid = true;
            } catch (NumberFormatException e) {
                System.out.print("  ¡La cantidad debe ser un número! Ingrese nuevamente: ");
                valid = false;
            } catch (NegativeArraySizeException e) {
                System.out.print("  ¡La cantidad no debe ser negativa! Ingrese nuevamente: ");
                valid = false;
            }
        } while (!valid);
        return tempNumbers;
    }

    private static void addItem(double result) {
        try {
            results[position] = result;
            position++;
            fileController.writeLine(String.valueOf(result));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("  ¡El límite del arreglo está fuera de rango!");
        }
    }

    private static void printHistory() {
        fileController.printContent();
    }

    private static Double readDoubleValid(int position) {
        double num = 0;
        boolean valid;
        do {
            try {
                System.out.print("  Ingrese el número " + (position + 1) + ": ");
                num = Double.parseDouble(keyboard.nextLine());
                valid = true;
            } catch (NumberFormatException e) {
                System.out.println("  ¡El valor ingresado no es decimal!");
                valid = false;
            }
        } while (!valid);
        return num;
    }

    private static int readInteger() throws NumberFormatException {
        String numberStr = keyboard.nextLine();
        return Integer.parseInt(numberStr);
    }
}
