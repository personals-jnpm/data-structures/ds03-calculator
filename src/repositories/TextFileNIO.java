package repositories;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class TextFileNIO {

    private final Path file;
    private final String END_LINE = System.getProperty("line.separator");

    public TextFileNIO(String fileName) throws IOException {
        file = Paths.get(fileName);
        if (!Files.exists(file)) {
            System.out.println("INFO: El archivo no existe, pero se creara automáticamente");
            Files.createFile(file);
        }
    }

    public void addRecord(String data) throws IOException {
        List<String> listLines;
        listLines = new ArrayList<>();
        listLines.add(data);
        Files.write(file, listLines, StandardOpenOption.APPEND);
    }

    public String[] getArray() throws IOException {
        String content;
        String[] listLines;

        listLines = new String[0];
        if (Files.size(file) > 0) {
            content = new String(Files.readAllBytes(file));
            listLines = content.split(END_LINE);
            return listLines;
        }
        System.out.println("El archivo esta vacío");
        return listLines;
    }

    public void reset() throws IOException {
        Files.delete(file);
        Files.createFile(file);
    }

    public void writeArray(String[] vec) throws IOException {
        reset();
        for (String s : vec) {
            addRecord(s);
        }
    }


}
