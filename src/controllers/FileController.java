package controllers;

import repositories.TextFileNIO;

import java.io.IOException;

public class FileController {
    private TextFileNIO textFileNIO;

    public FileController() {
        try {
            this.textFileNIO = new TextFileNIO("results.txt");
        } catch (IOException e) {
            System.out.println("El archivo no pudo ser creado");
        }
    }

    public void writeLine(String data){
        try {
            this.textFileNIO.addRecord(data);
        } catch (IOException e) {
            System.out.println("El archivo no pudo escribirse");
        }
    }

    public void printContent(){
        String[] array;
        try {
            array = textFileNIO.getArray();
            printArray(array);
        } catch (IOException e) {
            System.out.println("El archivo no pudo ser leído");
        }
    }

    private void printArray(String[] array) {
        for (String item : array) {
            System.out.println(item);
        }
    }



}
